# Koncepcja pracy dyplomowej

1. Proponowany tytuł pracy

2. Autor/rzy (imię i nazwisko każdego z nich, a dla email autora korespondującego)

3. Proponowany cel 

	- Na jakie pytanie ma odpowiedzieć ta praca?
	- Co autorzy chcą sprawdzić?
	- Czego autorzy chcą się nauczyć?

4. Proponowany zakres pracy (zaznacz wszystkie właściwe)

	- Zakres organizacyjny
		- cała firma
		- kilka zespołów/działów w firmie
		- jeden zespół/dział
		- pojedyncza osoba

	- Użyte narzędzia
		- Kanban
		- Analiza przedoprojektowa/narzędzia myślowe
			- Chmura 
				- pojedyncza
			- Drzewo celów
			- Drzewo stanu obecnego
			- 
	- Zakres procesu 
		- Przygotowanie 
		- Wdrożenie

- Kanban
- Proces przygotowawczy
- Sieć projektu

	- dla zespołu
	- w środowisku wieloprojektowym
	- dla jednego projektu
