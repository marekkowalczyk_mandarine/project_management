Efektywne zarządzanie projektami
-	
-	


Od 20 lat widzę te same problemy w projektach…
Projekty są spóźnione i/lub przekraczają budżet, przy jednoczesnym niedorobieniu zakresu i/lub słabej jakości 
— za późno, za drogo, za mało, za słabo
Dzieje się tak dużo, że wszyscy tracą nad tym kontrolę 
— projekty są nie do końca przygotowane, co się mści w realizacji, ale nie ma jak ich nadgonić: ciągłe zmiany priorytetów, latanie od pożaru do pożaru
Projekty, które do tej pory niby szły dobrze nagle okazują się być poważnie opóźnione 
— żarło, żarło i zdechło: zarząd nie ma wiarygodnych informacji o rzeczywistych postępach prac

Czym jest i skąd się bierze zławiel?
W jakim kierunku idzie rozwiązanie?
Jak wdrożyć rozwiązanie?
Jaki jest kolejny krok?

## Czym jest i skąd się bierze zławiel?

Istotną potrzebą biznesu jest zwiększenie tempa działania
Musimy prowadzić bardzo wiele projektów jednocześnie, bo tego wymaga od nas dzisiaj rynek
Musimy się spieszyć, i to na wielu frontach naraz
Ten projektu musimy zacząć już teraz, żeby zdążyć z prezentacją nowego wyrobu na przyszłoroczne targi

Wszystko co robimy, to przyglądanie się czasowi od momentu złożenia zamówienia przez klienta do momentu, gdy odbieramy gotówkę, i skracanie tego czasu — Taiichi Ohno, Toyota

Zapytajmy 100 managerów… Jeśli rozpoczęcie projektu przyspieszymy o 12 dni, to co się najprawdopodobniej stanie z zakończeniem tego projektu?

Eksperyment

Będą dwa podejścia:
1.	Start projektów z opóźnieniem
2.	Przyspieszamy start każdego z projektów
W którym przypadku projekty skończą się szybciej?

http://projektynaczas.pl/cwiczenie

Zławiel opóźnia realizację na kilka sposobów

1.	Straty na przełączanie się między zadaniami 
2.	Błędy, które trzeba potem naprawiać
3.	Zmęczenie, więc ludzie pracują wolniej
4.	Demotywacja
5.	Mniejsza kreatywność, gorsze rozwiązania
6.	Efekt „sałatki zadaniowej”

Opóźnienia
Zaczynanie ASAP
Zła wielozdadaniowość
Opóźnienia narastają
Im szybciej zacznę, tym szybciej skończę

Zła wielozadaniowość (zławiel) to sytuacja, w której jednoczesna realizacja zbyt dużej ilości pracy (zadań, projektów, zleceń) prowadzi do znacznego wydłużenia czasu i obniżenia jakości

Zławiel występuje wtedy, gdy notorycznie przerywa się pracę nad bieżącym zadaniem przed jego naturalnym końcem, aby zająć się innym, pilniejszym zadaniem

Żonglowanie

Powód osobisty: jestem wybitnym praktykiem zławiel (ADHD, hipomania). Gallup nazywa to talentem Aktywator. Astrolog — kwadraturą Słońca i Marsa. 

Powód biznesowy: od 20 lat zajmuję się zarządzaniem projektami i ciągle widzę te same problemy. Okazuje się, że zławiel to źródłowa przyczyna prawie wszystkich z nich. Chcę być jak Dr House — a nie leczyć dżumę biciem w dzwony.

Jak zdiagnozować zławiel?
W pracy fizycznej (szewc, mechanik, fryzjer) to bardzo łatwe — wchodzę i widzę pracę w toku (WIP)
Jak mieszkam przy skrzyżowaniu, to mogę wyjrzeć i widzę, czy jest korek

… W pracy niefizycznej to bardzo trudne — bo jej nie widać 
Musimy badać po symptomach, zadając jedno kluczowe pytanie…
(To pytanie zadaliśmy też respondentom w badaniu „Projekty Na Czas 2016”)

W pracy niefizycznej WIP jest niewidoczny i nie zajmuje miejsca

Łatwo dopuścić do jego wzrostu ponad miarę, ponieważ nie ma żadnej granicy ani naocznej informacji zwrotnej

Musimy więc to, co niewidoczne uczynić widocznym i sprawić że zacznie zajmować miejsce w przestrzeni — uzyskując naoczną informację zwrotną = kanban = visual management

(Jeśli ktoś ma notoryczną tendencję do popadania w zławiel, to może to być ADD/ADHD)

Ile raz w ciągu ostatniego tygodnia musiałeś przerwać pracę, by zająć się czymś pilniejszym?

Projekty Na Czas badanie: Zławiel dotyka aż 84 proc. badanych osób

FIRMA P. DANIELA

Woj. dolnośląskie
Ok. 100 osób
Na rynku od ok. 10 lat
Projektowanie i budowa maszyn pod klienta (ETO)
Ciągły wzrost sprzedaży
Bardzo rentowne projekty
Firma na skraju bankructwa
Dlaczego?!

Kiedy nie ma zławiel? Ilość przerwań   
≤ 3/miesiąc

Aż 80% projektów się opóźnia!

Terminowość w firmie p. Daniela: poniżej 5% do 25%

Kto sieje asap, ten zbiera fakap.

Przerwa na ciekawostkę
54 strategiczne projekty od … do …

Tezy o zławiel
Zławiel jest wrogiem nr 1
Zławiel jest powszechna
Zacznij od redukcji zławiel
Trzymaj się procesu

UWAGA!!!!   
To bardzo proste, ale bardzo trudne

Zarządzanie projektami nie istnieje, bo nie można zarządzać rzeczownikami abstrakcyjnymi. Jest praca z ludźmi. 

## W jakim kierunku idzie rozwiązanie?
Słonie w rurze, światełko, policjant
Golratt: Nie tylko pierwszy, ale również ostatni słoń wyjdzie znacznie szybciej, jeśli będą szły po kolei

Maksymalny przerób jest osiągany przy optymalnym poziomie WIP
Zagłodzenie, zławiel, optimum. Jeśli na czerwonym, a myślę, że na żółtym… TIR, Białystok urząd pracy

Tempo napływu   
niemożliwe do obsłużenia  
istniejącymi zasobami

Tempo napływu   
możliwe do obsłużenia  
istniejącymi zasobami

Zmiana poziomu WIP wskutek zamrożenia nadmiaru — wypalanie/zjadanie backlogu

Jeden z zespołów wsparcia technicznego w rozproszonej strukturze IT — Chwal (kejs osobno załączony) — dwa rysunki

Jeśli zwiększasz WIP szybciej, niż tempo przetwarzania,   
to wydłużasz czas oczekiwania.  
Czym dłuższa kolejka, tym większy czas stania
— Prawo Little’a w sklepie monopolowym

Aby zmniejszyć WIP (skrócić QT), dopasuj tempo napływu do tempa wypływu!

QT = WIP/T

Jeśli występuje zławiel… Jedynym sposobem przyspieszenia jest chwilowe wstrzymanie pracy nad istotną częścią projektów — ograniczenie WIP — i wprowadzanie „nowej” pracy w tempie wychodzenia „starej”
Zwykle zamraża się 50%   
(nie mniej niż 25%, lecz niekiedy aż 90%)

Symptomem (postrzeganym problemem) brak jest zasobów
Jednak dodanie zasobów nie przyspieszy, tylko podroży i dalej spowolni 
Dlaczego? Złożoność komunikacyjna, czas na wdrożenie, więcej ludzi = więcej inicjatyw. Kaczor: Co ja teraz zrobię z tymi ludźmi??

Zmiana składu zespołu R&D w Wielton — kejs audio

Jednak zwykle nikt nie zaczyna od zmniejszenia WIP…

Szkolenia
Certyfikacja
PRINCE2 
PMI
MS Project
Premie za projekty

Posłuchaj „Skoro jest tak dobrze, to dlaczego jest tak źle?” z II konferencji PMI we Wrocławiu   
http://projektynaczas.pl/skoro-jest-tak-dobrze  oraz artykuł

Czosnek
Ocet siedmiu złodziei
Driakiew
Procesje
Bicie w dzwony
Kaplica z czaszek

Co to jest kanban?

Wizualny system przedstawiania i kontroli ilości pracy w toku
Kartki reprezentują pracę 
Tablica i jej kolumny reprezentują system przetwarzający pracę
Celem kanbanu jest zmaksymalizowanie przepływu pracy przez system w jednostce czasu (przerób)
Rodzaj pracy nie ma znaczenia
Identyfikowalność etapów
Kartki „przepływają” przez kolejne kolumny symbolizując przechodzenie pracy z jednego etapu w drugi
Kanban może być narzędziem przejściowym lub docelowym
Słowo „kanban” jest bardzo wieloznaczne

Nie każdy „kanban” to kanban

„Mgławica”, wizualizacja, Do zrobienia, w trakcie zrobione, zamrożenie, praca nad zamrożonymi, potem nad nowymi…

Podstawowe zasady kanbanu
1. Zwizualizuj przepływ pracy
2. Ogranicz pracę w toku (WIP)
3. Bilansuj przepływ

4. Make Process Policies Explicit
5. Use Models* to Recognize Improvement Opportunities

1. Zwizualizuj przepływ pracy — Przedstaw etapy pracy i wszystkie zadania na tablicy suchościeralnej na ścianie

2. Ogranicz ilość pracy w toku — Ustal i przestrzegaj limit ilości zadań, nad którymi pracujecie w danej chwili

3. Zrównoważ przepływ — Śledź, czy zadania przepływają w równym, stabilnym tempie; odblokowuj zadania zablokowane; systemowo usuwaj przyczyny blokad

Limited WIP systems:
- kanban
- CONWIP
- SDBR

Kanban is a complex adaptive system for catalyzing incremental changes to produce a Lean organization

5 Core Properties: 

Visualize Workflow
Limit Work-in-Progress
Measure & Manage Flow
Make Process Policies Explicit
Use Models* to Recognize Improvement Opportunities*

Wyrób kanbanopodobny

Push Pull



## Definicja zławiel

Zła wielozadaniowość (zławiel) to sytuacja, w której jednoczesna realizacja zbyt dużej ilości pracy (zadań, projektów, zleceń) prowadzi do znacznego wydłużenia czasu i obniżenia jakości
Zławiel występuje wtedy, gdy notorycznie przerywa się pracę nad bieżącym zadaniem przed jego naturalnym końcem, aby zająć się innym, pilniejszym zadaniem

## Tezy o zławiel
Zławiel jest wrogiem nr 1
Zławiel jest powszechna
Zacznij od redukcji zławiel
Trzymaj się procesu

UWAGA!!!!   
To bardzo proste, ale bardzo trudne

Maksymalny przerób jest osiągany przy optymalnym poziomie WIP

Jeśli występuje zławiel…
Jedynym sposobem przyspieszenia jest chwilowe wstrzymanie pracy nad istotną częścią projektów — ograniczenie WIP — i wprowadzanie „nowej” pracy w tempie wychodzenia „starej”
Zwykle zamraża się 50%   
(nie mniej niż 25%, lecz niekiedy aż 90%)
Symptomem (postrzeganym problemem) brak jest zasobów
Jednak dodanie zasobów nie przyspieszy, tylko podroży i dalej spowolni 
Dlaczego?

Wizualny system przedstawiania i kontroli ilości pracy w toku
Kartki reprezentują pracę 
Tablica i jej kolumny reprezentują system przetwarzający pracę
Celem kanbanu jest zmaksymalizowanie przepływu pracy przez system w jednostce czasu (przerób)
Rodzaj pracy nie ma znaczenia
Identyfikowalność etapów
Kartki „przepływają” przez kolejne kolumny symbolizując przechodzenie pracy z jednego etapu w drugi
Kanban może być narzędziem przejściowym lub docelowym
Słowo „kanban” jest bardzo wieloznaczne

Nie każdy „kanban” to kanban

Podstawowe zasady kanbanu

1. Zwizualizuj przepływ pracy
2. Ogranicz pracę w toku (WIP)
3. Bilansuj przepływ

Przykładowe etapy przepływu
- Ogólnie: To Do — Doing — Done
- IT Development: Scoping — Design — Coding & Unit Testing — System Testing — User testing
- NPD Hi-Level Design — Low-Level Design — Virtual Testing — Prototyping — Physical Testing — Production Ramp-Up
- ETO System design — Detailed Design — Procurement — Manufacturing — Assembly — Testing
- MRO Inspection & Disassembly — Repair — Assembly — Inspection — Trials

## Proces redukcji zławiel

1.	Zidentyfikuj wszystkie projekty oraz etapy przepływu
2.	Odmroź i dozasobuj projekty priorytetowe
3.	Zidentyfikuj i odciąż integrację
4.	Zdefiniuj brakujące prace przygotowawcze
5.	Pracując wg zasad, nadganiaj przygotowania, kończ aktywne projekty i odmrażaj zamrożone 
6.	Opracuj proces przygotowawczy i uruchamiaj wg niego nowe projekty
7.	Bezlitośnie pilnując WIP zrób kolejny krok

## 1. Zidentyfikuj wszystkie projekty oraz etapy przepływu
1. Zbierz odpowiedni zespół
Upewnij się, że ludzie rozumieją konieczność zmiany
Ugruntuj zasadność ograniczenia ilości pracy w toku jako jedynego rozwiązania
2. Wypiszcie wszystko na żółtych karteczkach
Jedna rzecz na jednej karteczce
Wszystko, co jest w toku
Wszystko, co jest do zrobienia w przyszłości (pomysły/oferty i zobowiązania)
Treść karteczki powinna jednoznacznie specyfikować projekt lub odsyłać do jego dokładnej specyfikacji
3. Na białej tablicy suchościeralnej zróbcie tabelę obrazującą etapy przepływu projektów
Przyklejcie karteczki
Pogrupujcie je wg etapu zaawansowania prac, od lewej do prawej
Narysujcie i nazwijcie kolumny
Typową reakcją jest przerażenie ilością pracy w toku — to dobrze!
4. Dla dodefiniowania granic, napiszcie kryteria, jakie musi spełniać projekt, by go przenieść z kolumny do kolumny

## 2. Odmroź i dozasobuj projekty priorytetowe
1. Weź małe karteczki („języczki”) w tylu kolorach, ile jest rodzajów zasobów
2. Dla każdej osoby ustalacie, nad iloma zadaniami jest w stanie pracować jednocześnie = ile karteczek z jej nazwiskiem jest w puli (zwykle 1-3 — więcej dla prac nadzorczych)
3. Poczynając od projektów priorytetowych, do każdej karteczki symbolizującej projekt przyklejacie odpowiednią ilość „języczków” zasobowych — tak żeby praca szła jak najszybciej (maksymalny sensowny przydział zasobów)
Unikamy cienkiego posmarowania zadań zasobami
Unikamy emitowania karteczek bez pokrycia
4. Projekty, dla których nie starczy „języczków” są zamrożone (wiszą pod kreską)
Upewnij się, że zamrożono przynajmniej 25% obciążenia pracą całego systemu
Trzeba fizycznie uniemożliwić ludziom pracę nad projektami zamrożonymi — i to ciągle monitorować
Bezlitośnie eliminuj chupchiki

Zamrażanie czy odmrażanie?
Jeśli pracujecie w trybie zławiel, to i tak co chwila zamrażacie projekty, ale w sposób chaotyczny — bez zwracania uwagi na ich priorytety
Przydzielenie zasobów do nieprzerwanej pracy najpierw nad projektami priorytetowymi porządkuje ten stan rzeczy i pozwala permanentnie odmrozić najważniejsze projekty

Kahneman & Tversky, Loss Aversion Bias
Wolisz 90% czy 10%

## 3. Zidentyfikuj i odciąż integrację
1. Zidentyfikuj, który z etapów jest integracją — etapem, który wywołuje najwięcej problemów
Cały system pracuje w tempie pracy najwolniejszego ogniwa
Takim etapem jest integracja — miejsce, gdzie zbiegają się różne ścieżki projektu
To miejsce generuje problemy losowo zasysając najbardziej wykwalifikowane zasoby z poprzednich etapów oraz zasoby zarządcze
Integacja będzie dyktować tempo odmrażania i uruchamiania nowych projektów
Integrację poznasz po tym, że to miejsce, gdzie naraz może być najmniej projektów
2. Upewnij się, że zamrożono przynajmniej 25% obciążenia integracji (drugi etap zamrażania)
Podobnie całego systemu — przynajmniej 25% mniej obciążenia
Zdarza się, że zamraża się nawet 80-90% pracy w toku

## 4. Zdefiniuj brakujące prace przygotowawcze
1. Behawioralnie doprecyzuj kryteria przejścia między kolumnami tworząc precyzyjne listy kontrolne
Używaj wyłącznie słów konkretnych 
Co mogłaby zarejestować kamera?
Braki powodują zławiel i poprawki!
2. Stwórz system rejestrowania postępów w nadganianiu prac przygotowawczych
Opracuj matrycę kryteria/projekty — będziesz na niej śledzić postępy w uzupełnianiu prac przygotowawczych
Mniej wygodne są osobne karty dla każdego etapu/projektu, choć czasem niezbędne
3. Będąc podejrzyliwym i brutalnie szczerym, przeciągnij każdy projekt przez każdą odpowiednią listę kontrolną, na osobnych karteczkach wypisując zadania nadganiające
4. Rekomendowane jest uruchomienie zespołowego kanbanu zadaniowego
Będzie śledzić zadania przygotowawcze i zasadnicze
Będzie gasił wrzutki i cofki
5. Zweryfikuj przydział zasobów do projektów, uwzględniając zadania przygotowawcze
W pierwszej kolejności zasoby do nadganiania prac przygotowawczych na projektach otwartych, następnie na zamrożonych
6. Na razie nie wolno dotykać nowych projektów — póki nie zakończy się znacznej większości prac na projektach aktywnych i zamrożonych

## 5. Pracując wg zasad, nadganiaj przygotowania, kończ aktywne projekty i odmrażaj zamrożone 

1. Spiszcie zasady pracy i się ich trzymajcie — zamiast ignorować modyfikujcie je wspólnie 
2. Bezustannie nadganiajcie prace przygotowawcze najpierw na projektach aktywnych, a następnie na zamrożonych — ściśle wg priorytetów
Korzystaj z kanbanu zadaniowego dla odtykania blokad na zadaniach
Rekomendowane jest śledzenie postępów w nadganianiu prac przygotowawczych przy pomocy wykresu wypalania (burndown chart)
Monitoruj niepracowanie nad projektami zamrożonymi
3. Codzienna praca z kanbanami
Mają postać fizyczną
Wiszą w miejscu widocznym dla wszystkich, zwłaszcza dla kierownictwa
Codzienna aktualizacja: najpierw kanban projektowy, potem zadaniowy
Szybkie poranne spotkanie na stojąco z użyciem czeklisty 
Obserwacje niepokojących zjawisk i bieżące rozwiązywanie problemów: zadania zablokowane — odnotowywanie i zbieranie powodów
Śledzenie liczby przerwań i odpowiednie interwencje
Przekroczenie limitu WIP — zamrozić czy zwiększyć limit WIP?
Zagłodzenie integracji — za mały WIP?, ostrożne zwiększanie??
4. Odmrażaj (i uruchamiaj nowe) projekty wg priorytetów w tempie zwalniania się języczków oraz wychodzenia projektów z integracji (pull, a nie push)
Jeśli projekt wyszedł z danego etapu, to „języczki” wracają do puli
Nie można uruchomić projektu nie mając wolnych „języczków” oraz zwolnionego miejsca w integracji
Uwolnione zasoby wykorzystaj do nadganiania prac przygotowawczych (przed chwilą narzekałeś, że masz za mało — teraz że za dużo)
Oprzyj się pokusie zbyt szybkiego uruchamiania nowych projektów, żeby dać coś ludziom do roboty

## 6. Opracuj proces przygotowawczy i uruchamiaj wg niego nowe projekty

1. Gdy już masz na to czas, zaplanuj systematyczny proces przygotowawczy dla nowych projektów
Czasem konieczne jest dodanie w projektach etapu przygowawczego, który będzie zarządzany projektowo
W ramach tego procesu konieczna jest analiza biznesowa wg metod rachunkowości przerobowej
Jeśli masz do czynienia z projektami o relatywnie stabilnym zakresie (a tak niemal na pewno jest), to warto aby przygotowanie nowych projektów obejmowało stworzenie sieci projektów wg szablonów, a następnie uruchomienie pełnego wdrożenia CCPM
Przygotowanie prawidłowych szablonów sieci projektów jest proste, lecz niełatwe i bardzo czasochłonne
2. Gdy zostanie zrealizowana znacząca większość (ok. 70%) prac przygotowawczych na aktywnych i zamrożonych projektach (wg burndown chart) zacznij przygotowywać nowe projekty wg ustalonego procesu
## 7. Bezlitośnie pilnując WIP zrób kolejny krok


1.	W każdej organizacji jest naturalna tendencja do wzrostu WIP — należy się jej ciągle i zdecydowanie przeciwstawiać
2.	Kanban nie daje gwarancji terminowości (95%+ na czas, bez uszczuplenia zakresu i/lub przekroczenia budżetu)
3.	Gdy to uzasadnione biznesowo należy (w środowiskach projektowych) poważnie rozważyć pełne wdrożenie metody łańcucha krytycznego (CCPM)
4.	Nie jest to możliwe bez dedykowanych narzędzi informatycznych i profesjonalnego wsparcia
1.	Istnieje olbrzymie niezrozumienie co to jest środowisko projektowe: decyduje obiektywna „zawartość pracy w pracy”, a nie subiektywne kryteria „innowacyjności” itp.; dyskusje projekt-proces są absurdalne 
2.	Optymalizacja przepływu w każdym środowisku to wdrożenie (w różny sposób) tych samych CZTERECH ZASAD — łańcuch krytyczny jest rozwiązaniem dedykowanym do konkretnych warunków; w innych warunkach należy korzystać z analogicznych rozwiązań dedykowanych dla danego środowiska

ZASADA 1
Mało projektów jednocześnie
ZASADA 2
Krótkie planowane czasy projektów
ZASADA 3
Przestrzeganie priorytetów zadań

Proces ciągłego doskonalenia (POOGI) wg Goldratta
0. Ustal CEL i sposób pomiaru

	1.	Zidentyfikuj ograniczenie(a) systemu
	2.	Zdecyduj, jak wyzyskać ograniczenie(a) systemu
	3.	Podporządkuj wszystko powyższej decyzji
	4.	Wywinduj ograniczenie(a) systemu
	5.	Uwaga!!!! Jeśli w którymś z poprzednich kroków ograniczenie zostało, wróć do kroku 1; nie pozwól, by ograniczeniem systemu stała się inercja

Zławiel = marnowanie ograniczenia

Cztery zasady zarządzania przepływem
1.	Poprawa przepływu (lub — co równoważne — czasu przetwarzania) jest głównym celem operacji
2.	Ten główny cel należy przełożyć na praktyczny mechanizm mówiącym operacjom, kiedy nie produkować (zapobieganie nadprodukcji)
3.	Należy zaprzestać poszukiwania lokalnej wydajności
4.	Należy uruchomić proces skupiania uwagi dla zrównoważenia przepływu
Sposoby wdrożenia zasad zarządzania przepływem w różnych środowiskach
Linia produkcyjna Forda
Toyota Production System (Lean)
Werbel-bufor-lina (DBR)
Łańcuch krytyczny (CCPM)
Replenishment
…

Ogólne zasady zarządzania przepływem są IDENTYCZNE we wszystkich środowiskach

Szczegóły ich wdrożenia są na pierwszy rzut oka zupełnie RÓŻNE

Pierwszym krokiem zawsze jest redukcja WIP (wygaszenie ZŁAWIEL)


# Na co należy uważać?
Rura (strumień wartości)  
na przykładzie MTO

Jak objawia się zławiel na każdym z etapów? 
Od którego z nich zacząć redukcję zławiel?
Na czym polega kluczowa różnica we wdrożeniu czterech zasad w każdym z tych środowisk?

Proces ciągłego doskonalenia (POOGI) wg Goldratta

0. Ustal CEL i sposób pomiaru

	1.	Zidentyfikuj ograniczenie(a) systemu
	2.	Zdecyduj, jak wyzyskać ograniczenie(a) systemu
	3.	Podporządkuj wszystko powyższej decyzji
	4.	Wywinduj ograniczenie(a) systemu
	5.	Uwaga!!!! Jeśli w którymś z poprzednich kroków ograniczenie zostało, wróć do kroku 1; nie pozwól, by ograniczeniem systemu stała się inercja

Zławiel = marnowanie ograniczenia

Chupchik
Projekt, który bezpośrednio nie daje wyraźnych korzyści — są one ujemne lub marginalne
Pośrednio chupchik wywołuje 
złą wielozadaniowość wśród kierownictwa firmy
poczucie, że tak wiele robimy dla firmy… 
Główny wróg zarządzania
Don’t just do something; stand there!

Sekwencja zmiany
1.	Co zmienić?
2.	Na co to zmienić?
3.	Jak spowodować zmianę?