1. Zidentyfikuj wszystkie projekty oraz etapy przepływu
1. Odmroź i dozasobuj projekty priorytetowe
1. Zidentyfikuj i odciąż integrację
1. Zdefiniuj brakujące prace przygotowawcze
1. Pracując wg zasad, nadganiaj przygotowania, kończ aktywne projekty i odmrażaj zamrożone 
1. Opracuj proces przygotowawczy i uruchamiaj wg niego nowe projekty
1. Bezlitośnie pilnując WIP zrób kolejny krok