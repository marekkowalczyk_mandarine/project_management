# Jak utrudnić sobie życie?


## Wyznawanie irracjonalnych przekonań

- Muszę ciągle odnosić sukcesy

_- Inni muszą być dla mnie dobrzy

- Okoliczności muszą mi zawsze sprzyjać

## Popełnianie kardynalnych błędów w relacjach z sobą i innymi

- Koncentrowanie się na swoich słabych stronach

- Mierzenie innych swoją miarą

- W relacji szukanie dziury w całym

## Popadanie w zławiel

- Za dużo pól odpowiedzialności

- Za dużo otwartych zadań/projektów

- Za dużo przeszkadzajek, wrzutek i cofek
